# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from . import forms
from django import forms
from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from Django_JobTracker.task_manager import TaskManager
from Django_JobTracker.settings import DATABASES
from Django_JobTracker.storage import Storage
from Django_JobTracker.methods import get_head_tasks, get_groups, get_filtered_tasks, get_list_of_date


def new_task(request):

    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        args['task_form'] = forms.TaskForm()
        args['task_form'].fields['group'] = forms.ChoiceField(choices=get_groups(User.get_username(request.user)),
                                                              label='Group')
        args['username'] = auth.get_user(request).username
        if request.POST:
            storage = Storage(DATABASES['default']['NAME'])
            task_manager = TaskManager(storage)
            new_task_form = forms.TaskForm(request.POST)
            if new_task_form.is_valid():
                task = new_task_form.cleaned_data
                if request.POST.get('new_group'):
                    task.update({'group': request.POST.get('new_group')})
                task.update({'executor': User.get_username(request.user)})
                try:
                    task_id = task_manager.new_task(task)
                except:
                    return render(request, 'new_task/new_task.html', args)
            else:
                args['task_form'] = new_task_form
        return render(request, 'new_task/new_task.html', args)
    else:
        return redirect('DJ/views/tasks_view')

