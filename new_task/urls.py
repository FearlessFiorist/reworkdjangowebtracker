from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.new_task, name='new_task'),
]
