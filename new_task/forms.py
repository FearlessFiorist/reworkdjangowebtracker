from django import forms

from DJ.models import Task


class TaskFilterForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['priority', 'status', 'date']


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ['executor', 'status', 'date']
