from django.conf.urls import url
from django.views.generic import ListView, DetailView
from DJ.models import Task
from . import views

urlpatterns = [
    #  url(r'^$', views.index, name='index')
    url(r'^$', views.tasks_view, name="task_all"),
    #  url(r'^new/$', views.new, name='new'),
    url(r'^(?P<pk>\d+)$', DetailView.as_view(model=Task, template_name="DJ/tasks_v2.html"))
]
