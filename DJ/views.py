# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.template.context_processors import csrf
import datetime
from DJ.forms import TaskFilterForm
from DJ.models import Task


def get_filtered_tasks(task_list, filter_fields):

    out_list = task_list
    if filter_fields.get('date'):
        date_f = filter_fields.get('date').strftime('%Y-%m-%d').split(' ')[0]
        out_list = [x for x in out_list if x.date.strftime('%Y-%m-%d') == date_f]
    if filter_fields.get('priority'):
        out_list = [x for x in out_list if x.priority == filter_fields.get('priority')]
    if filter_fields.get('status'):
        out_list = [x for x in out_list if x.status == filter_fields.get('status')]
    return out_list


def tasks_view(request):

    content = {}
    content.update(csrf(request))

    # tasks = Tasks.objects.all().order_by("-name")[:20]
    date = datetime.datetime(2018, 5, 5)
    test_task = Task()
    test_task.name = 'name'
    test_task.content = 'content'
    test_task.priority = 3
    test_task.date = date
    test_task.executor = 'executor'
    test_task.status = 'In_progress'
    test_task.written_by = 'executor'
    test_task.list_of_sub_tasks = ''
    tasks = list()
    tasks.append(test_task)

    filter_form = TaskFilterForm(request.GET)
    if filter_form.is_valid():
        filter_fields = filter_form.cleaned_data
        tasks = get_filtered_tasks(tasks, filter_fields)
    content['form'] = filter_form
    content.update({'tasks': tasks})

    return render(request, 'new/tasks_v2.html', content)
