# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-16 16:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DJ', '0005_auto_20180810_0942'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='list_of_sub_tasks',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='task',
            name='written_by',
            field=models.CharField(default=models.CharField(max_length=100), max_length=100),
        ),
    ]
