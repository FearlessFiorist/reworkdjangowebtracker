# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Task(models.Model):
    PRIORITIES = ((3, 'Low'), (2, 'Medium'), (1, 'High'))
    STATUSES = (('Failed', 'Failed'), ('In_progress', 'In progress'))

    name = models.CharField(max_length=100)
    content = models.TextField()
    priority = models.IntegerField(choices=PRIORITIES, blank=True)
    date = models.DateTimeField(blank=True)
    executor = models.CharField(max_length=100)
    status = models.CharField(choices=STATUSES, blank=True, max_length=20)

    written_by = models.CharField(max_length=100, default=executor)
    list_of_sub_tasks = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.name
