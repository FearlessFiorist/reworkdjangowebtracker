#!/home/picasso/git/Django_JobTracker/django_jobtracker/venv/bin/python
from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
