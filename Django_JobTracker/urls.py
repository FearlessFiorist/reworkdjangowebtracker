from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^new_task/', include('new_task.urls')),
    url(r'^DJ/', include('DJ.urls')),
    url(r'^', include('mainApp.urls')),
]
