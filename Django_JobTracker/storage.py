#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from Django_JobTracker.task import Task
from datetime import datetime, timedelta
from peewee import *


_database_proxy = Proxy()


class BaseTableClass(Model):
    class Meta:
        database = _database_proxy


class TaskTable(BaseTableClass):
    name = CharField(primary_key=True)
    content = CharField()
    group = CharField(max_length=30, default='Personal')
    priority = IntegerField(default=3, choices=range(1, 4))
    date_of_execution = DateTimeField(default=datetime.now() + timedelta(days=1))
    executor = CharField()
    status = CharField(default='In_progress', max_length=30)
    written_by = CharField(max_length=30)
    list_of_sub_tasks = CharField(max_length=30)

    class Meta:
        db_table = 'task_task'


class Storage:

    def __init__(self, db_path):
        self.db = SqliteDatabase(db_path)
        _database_proxy.initialize(self.db)

    def get_all_tasks(self, user_name):

        tasks = []
        for task in TaskTable.select().where(TaskTable.executor == user_name):
            tasks.append(self.to_task(task))
        return tasks

    @staticmethod
    def to_task(table_task):

        task = Task(table_task.name, table_task.content, table_task.group, table_task.priority,
                    table_task.date_of_execution, table_task.executor, table_task.status,
                    table_task.written_by, table_task.list_of_sub_tasks)
        task.name = table_task.name
        task.status = table_task.status
        task.date_of_execution = table_task.date_of_execution
        return task

    # @staticmethod
    # def path_set(value):
    #     if path_check(value):
    #         return value
    #     else:
    #         raise exceptions.ConfigError('Wrong path')

    def load_user_tasks(self, user_name):

        tasks = []
        for task in TaskTable.select().where(TaskTable.executor == user_name):
            tasks.append(self.to_task(task))
        return tasks

    @staticmethod
    def add_new_task(item):

        table = TaskTable()
        table.name = item.get('name')
        table.content = item.get('content')
        table.group = item.get('group')
        table.priority = item.get('priority')
        table.date_of_execution = item.get('date_of_execution')
        table.executor = item.get('executor')
        table.status = item.get('status')
        table.written_by = item.get('written_by')
        table.list_of_sub_tasks = item.get('list_of_sub_tasks')
        table.save()
        return table.name

    @staticmethod
    def pop_task(item):

        table = TaskTable.get(TaskTable.name == item.name)
        table.delete_instance()

    @staticmethod
    def update_existed_task(item):

        table = TaskTable.select().where(TaskTable.name == item.name).get()
        table.content = item.content
        table.group = item.group
        table.priority = item.priority
        table.date_of_execution = item.date_of_execution
        table.status = item.status
        table.list_of_sub_tasks = item.list_of_sub_tasks
        table.save()

    def load_task_by_id(self, task_name):

        table_task = TaskTable.select().where(TaskTable.name == task_name).get()
        task = self.to_task(table_task)
        return task
