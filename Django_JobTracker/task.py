#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


class Task:

    def __init__(self, name, condition, group, priority, date_of_execution, executor, written_by,
                 status, list_of_sub_tasks):
        self.name = name
        self.condition = None
        self.group = None
        self.priority = None
        self.date_of_execution = None
        self.executor = executor
        self.status = "In_progress"
        self.written_by = None
        self.list_of_sub_tasks = None
