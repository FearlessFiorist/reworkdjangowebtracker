#!/usr/bin/venv python3.5
# -*- coding: utf-8 -*-

from datetime import date, datetime
from Django_JobTracker.storage import Storage
from Django_JobTracker.settings import DATABASES


def get_head_tasks(user_name):

    storage = Storage(DATABASES['default']['NAME'])
    tasks = [(str(0), str(0))]
    for task in storage.load_user_tasks(0, user_name):
        tasks.append((str(task.id), str(task.id)))
    if tasks:
        return tuple(tasks)


def get_groups(user_name):

    storage = Storage(DATABASES['default']['NAME'])
    groups = [('Personal', 'Personal'),
              ('Work', 'Work')]
    for task in storage.get_all_tasks(user_name):
        if tuple((task.group, task.group),) not in groups:
            groups.append((task.group, task.group))
    if groups:
        return tuple(groups)


def get_filtered_tasks(task_list, filter_fields):

    out_list = task_list
    if filter_fields.get('execution_date'):
        date_f = filter_fields.get('execution_date').split(' ')[0]
        out_list = [x for x in out_list if x.execution_date.strftime('%Y-%m-%d') == date_f]
    if filter_fields.get('group'):
        out_list = [x for x in out_list if x.group == filter_fields.get('group')]
    if filter_fields.get('priority'):
        out_list = [x for x in out_list if x.priority == filter_fields.get('priority')]
    if filter_fields.get('status'):
        out_list = [x for x in out_list if x.status == filter_fields.get('status')]
    return out_list


def get_list_of_date(tasks):

    ex_date = [('', '--------'), ]
    for task in tasks:
        task_date = datetime.strptime(task.execution_date.strftime('%d-%B-%Y'), '%d-%B-%Y')
        if tuple((task_date, task.execution_date.strftime('%d-%B-%Y')),) not in ex_date:
            # noinspection PyTypeChecker
            ex_date.append((task_date, task.execution_date.strftime('%d-%B-%Y')))
    return tuple(ex_date)
